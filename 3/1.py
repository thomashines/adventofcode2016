#!/usr/bin/env python3

import sys

valid = 0
for l in sys.stdin:
    a, b, c = sorted(int(s) for s in l.split(" ") if len(s) > 0)
    if a + b > c:
        valid += 1
print(valid)
