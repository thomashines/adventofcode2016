#!/usr/bin/env python3

import sys

valid = 0
stack = []
for l in sys.stdin:
    stack.append(tuple(int(s) for s in l.split(" ") if len(s) > 0))
    if len(stack) == 3:
        for i in range(3):
            a, b, c = sorted(stack[j][i] for j in range(3))
            if a + b > c:
                valid += 1
        stack = []
print(valid)
