#!/usr/bin/env python3

import json
import sys

def deshift(text, key):
    deshifted = ""
    for c in text:
        if c == "-":
            deshifted += " "
        else:
            min_d = ord("a")
            max_d = ord("z")
            c_d = ord(c) - min_d
            rot = c_d + key
            modded = rot % (max_d - min_d + 1)
            deshifted += chr(min_d + modded)
    return deshifted

rooms = []
for l in sys.stdin:
    id_parts = l[:l.index("[")].split("-")
    num = int(id_parts[-1])
    id = "-".join(id_parts[:-1])
    checksum = l[l.index("[") + 1:-2]
    counts = {}
    for c in id:
        if c != "-":
            if c in counts:
                counts[c] += 1
            else:
                counts[c] = 1
    counts = ((-v, k) for (k, v) in counts.items())
    real_checksum = "".join(c[1] for c in sorted(counts)[:5])
    if checksum == real_checksum:
        rooms.append((deshift(id, num), num))
print(sum(r[1] for r in rooms))

for id, num in rooms:
    if "north" in id:
        print(id, num)

# words = {}
# with open("words_dictionary.json", "r") as f:
#     words = json.loads(f.read())

# for id, num in rooms:
#     for word in id.split(" "):
#         if word in words:
#             print(id, num)
#             break
