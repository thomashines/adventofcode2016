#!/usr/bin/env python3

import sys

pad = (
    (None, None, "1", None, None),
    (None, "2", "3", "4", None),
    ("5", "6", "7", "8", "9"),
    (None, "A", "B", "C", None),
    (None, None, "D", None, None),
)
x, y = 0, 2
code = []
for l in sys.stdin:
    for m in l.strip():
        dx, dy = {"U": (0, -1), "D": (0, +1), "L": (-1, 0), "R": (+1, 0)}[m]
        x2, y2 = x + dx, y + dy
        if x2 >= 0 and x2 <= 4 and y2 >= 0 and y2 <= 4 and pad[y2][x2] is not None:
            x, y = x2, y2
    code.append(pad[y][x])
print("".join(str(n) for n in code))
