#!/usr/bin/env python3

import sys

pad = tuple(tuple(range(3 * i + 1, 3 * i + 4)) for i in range(3))
x, y = 1, 1
code = []
for l in sys.stdin:
    for m in l.strip():
        dx, dy = {"U": (0, -1), "D": (0, +1), "L": (-1, 0), "R": (+1, 0)}[m]
        x2, y2 = x + dx, y + dy
        if x2 >= 0 and x2 <= 2 and y2 >= 0 and y2 <= 2:
            x, y = x2, y2
    code.append(pad[y][x])
print("".join(str(n) for n in code))
