#!/usr/bin/env python3

import sys

x, y = 0, 0
dx, dy = 1, 0
visited = set((0, 0))
for l in sys.stdin:
    for m in l.strip().split(", "):
        if m[0] == "L":
            dx, dy = dy, -dx
        elif m[0] == "R":
            dx, dy = -dy, dx
        for _ in range(int(m[1:])):
            x, y = x + dx, y + dy
            if (x, y) in visited:
                print(abs(x) + abs(y))
            else:
                visited.add((x, y))
print(abs(x) + abs(y))
